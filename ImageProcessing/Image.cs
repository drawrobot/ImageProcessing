﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ImageProcessing
{
    public class Image
    {
        public Bitmap Picture { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }

        public double SEPARATOR = 0.8;
        private int BLOCK_SIZE = 6;

        public Image(string path, int preferredWidth, int preferredHeight)
        {
            Picture = new Bitmap(path);
            Width = preferredWidth / BLOCK_SIZE;
            Height = preferredHeight / BLOCK_SIZE;
            Console.WriteLine(ToString());
        }
        public Image(string path, int preferredWidth)
        {
            Picture = new Bitmap(path);
            Width = preferredWidth / BLOCK_SIZE;
            using (Bitmap bitmap = new Bitmap(path))
            {
                Height = preferredWidth*bitmap.Height/bitmap.Width / BLOCK_SIZE;
            }
            Console.WriteLine(ToString());
        }

        public Image(Bitmap picture, int preferredWidth)
        {
            Picture = picture;
            Width = preferredWidth / BLOCK_SIZE;
            Height = preferredWidth * picture.Height / picture.Width / BLOCK_SIZE;
            Console.WriteLine(ToString());
        }

        public override string ToString() => $"Picture size will be: x: {Width * BLOCK_SIZE}mm * y: {Height * BLOCK_SIZE}mm";

        public List<short> DrawImage(Pen pen)
        {
            bool[,] result = PrecalculateImage();
            List<short> commands = new List<short>();
            Point startPoint = Program.CalcPoint(pen.L, pen.R);
            for (int y = 0; y < result.GetLength(1); y++)
            {
                int x = 0;
                for (; x < result.GetLength(0); x++) // ->
                {
                    if (result[x, y])
                    {
                        DrawBlock(pen, commands, x, startPoint, y);
                    }
                    else
                    {
                        commands.AddRange(pen.SegmentedLineTo(new Point((x + 1) * BLOCK_SIZE + startPoint.X, y * BLOCK_SIZE + startPoint.Y)));
                    }
                }
                y++;
                if (y == Height) break;
                commands.AddRange(pen.SegmentedLineTo(new Point(x * BLOCK_SIZE + startPoint.X, y * BLOCK_SIZE + startPoint.Y)));
                x--;
                for (; x > 0; x--) // <-
                {
                    if (result[x, y])
                    {
                        DrawBlockBackwards(pen, commands, x, startPoint, y);
                    }
                    else
                    {
                        commands.AddRange(pen.SegmentedLineTo(new Point((x+1) * BLOCK_SIZE + startPoint.X, y * BLOCK_SIZE + startPoint.Y)));
                    }
                }
                commands.AddRange(pen.SegmentedLineTo(new Point(startPoint.X, y * BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(startPoint.X, (y+1) * BLOCK_SIZE + startPoint.Y)));
            }
            return commands;
        }

        private void DrawBlockBackwards(Pen pen, List<short> commands, int x, Point startPoint, int y)
        {
                double currX = x * BLOCK_SIZE + startPoint.X;
                commands.AddRange(pen.SegmentedLineTo(new Point(currX, y* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX, (y - 1)* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX - BLOCK_SIZE / 2,(y - 1)* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX - BLOCK_SIZE / 2,y* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX - BLOCK_SIZE, y* BLOCK_SIZE + startPoint.Y)));
        }

        private void DrawBlock(Pen pen, List<short> commands, int x, Point startPoint, int y)
        {
                double currX = x*BLOCK_SIZE + startPoint.X;
                commands.AddRange(pen.SegmentedLineTo(new Point(currX, y* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX, (y - 1)* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX + BLOCK_SIZE / 2,(y - 1)* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX + BLOCK_SIZE / 2,y* BLOCK_SIZE + startPoint.Y)));
                commands.AddRange(pen.SegmentedLineTo(new Point(currX + BLOCK_SIZE, y* BLOCK_SIZE + startPoint.Y)));
        }

        public bool[,] PrecalculateImage()
        {
            bool[,] result = new bool[Width, Height];
            Bitmap bitmap = Picture;
            bitmap = new Bitmap(bitmap.GetThumbnailImage(Width, Height, null, IntPtr.Zero));
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    double tmp = (bitmap.GetPixel(x, y).GetBrightness());
                    if (tmp < SEPARATOR)
                        result[x,y] = true;
                }
            }
            return result;
        }
    }
}
