﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Design;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing
{
    public class Pen
    {
        public double L { get; set; }
        public double R { get; set; }

        //U = 2*pi*r  
        // 10mm
        //1 steps per step
        public static double Step = ((2.0 * Math.PI * 7.5) / 4096.0) * 8;
        public bool IsDrawing { get; set; }

        /// <summary>
        /// Calculates the distance between to points
        /// </summary>
        /// <param name="a">point a</param>
        /// <param name="b">point b</param>
        /// <param name="realValue">if the return should be the real distance, or the square number of the distance is enough (performance improvements)</param>
        /// <returns></returns>
        public double LengthFromTwoPoints(Point a, Point b, bool realValue = true)
        {
            double x = a.X - b.X;
            double y = a.Y - b.Y;
            if (realValue)
            {
                return Math.Sqrt(x * x + y * y);
            }
            return x * x + y * y;
        }

        struct Option
        {
            public double l;
            public double r;
            public short command;
        }

        /// <summary>
        /// LineTo
        /// 
        /// At every point the system has 4 options to move (lup, rup, ldown, rdown)
        /// So we check at each point these options and if we go this option and we are nearer to the goal
        /// it will be added to the queue. 
        /// Normally we get 2 options. Now we check this options and select the better choose to go.
        /// </summary>
        /// <param name="p">Destination</param>
        public List<short> SegmentedLineTo(Point p)
        {
            Point a = Program.CalcPoint(L,R);

            double length = LengthFromTwoPoints(a, p, true);
            int segments = Convert.ToInt32(length * 10);
            double deltaX = (p.X - a.X) / segments;
            double deltaY = (p.Y - a.Y) / segments;

            List<short> commands = new List<short>();
            Point newPoint = new Point();
            for (var i = 1; i <= segments; i++)
            {
                newPoint.X = a.X + deltaX*i;
                newPoint.Y = a.Y + deltaY*i;

                commands.AddRange(LineTo(newPoint));
            }
            commands.AddRange(LineTo(p));
            //Console.WriteLine(p);
            return commands;
        }

        private List<short> LineTo(Point p)
        {
            Point currPosition = Program.CalcPoint(L, R);
            List<short> commands = new List<short>();
            double tmpl = L;
            double tmpr = R;
            while (!currPosition.Equals(p))
            {
                currPosition = Program.CalcPoint(tmpl, tmpr);
                Queue<Option> s = new Queue<Option>();
                double currToDest = LengthFromTwoPoints(currPosition, p);
                if (currToDest > LengthFromTwoPoints(Program.CalcPoint(tmpl + Step, tmpr), p))//0
                {
                    s.Enqueue(new Option() { command = 0, l = Step, r = 0 });
                }
                if (currToDest > LengthFromTwoPoints(Program.CalcPoint(tmpl - Step, tmpr), p))//1
                {
                    s.Enqueue(new Option() { command = 1, l = -Step, r = 0 });
                }
                if (currToDest > LengthFromTwoPoints(Program.CalcPoint(tmpl, tmpr + Step), p))//2
                {
                    s.Enqueue(new Option() { command = 2, l = 0, r = Step });
                }
                if (currToDest > LengthFromTwoPoints(Program.CalcPoint(tmpl, tmpr - Step), p))//3
                {
                    s.Enqueue(new Option() { command = 3, l = 0, r = -Step });
                }
                if (s.Count > 2)
                    throw new Exception();
                if (s.Count == 1)
                {
                    commands.Add(s.First().command);
                    tmpl += s.First().l;
                    tmpr += s.First().r;
                    currPosition = Program.CalcPoint(tmpl, tmpr);
                    if (!currPosition.Equals(p))
                    {
                        continue;
                    }
                    L = tmpl;
                    R = tmpr;
                    return commands;
                }
                if (s.Count == 0)
                {
                    L = tmpl;
                    R = tmpr;
                    return commands;
                }
                Point a = Program.CalcPoint(tmpl + s.First().l, tmpr + s.First().r);
                Point b = Program.CalcPoint(tmpl + s.Last().l, tmpr + s.Last().r);
                if (IsNearerThanPoint(a, b, Program.CalcPoint(L, R), p))
                {
                    commands.Add(s.First().command);
                    tmpl += s.First().l;
                    tmpr += s.First().r;
                }
                else
                {
                    commands.Add(s.Last().command);
                    tmpl += s.Last().l;
                    tmpr += s.Last().r;
                }
                currPosition = Program.CalcPoint(tmpl, tmpr);
            }
            L = tmpl;
            R = tmpr;
            return commands;
        }

        public bool IsNearerThanPoint(Point a, Point b, Point lineStart, Point lineEnd)
        {
            return DotLineLength(a, lineStart, lineEnd) < DotLineLength(b, lineStart, lineEnd);
        }

        /// <summary>
        /// Calculate the distance between the given point and the line
        /// </summary>
        /// <param name="a">arbitrary point</param>
        /// <param name="b">lineStart</param>
        /// <param name="c">lineEnd</param>
        /// <returns></returns>
        public double DotLineLength(Point a, Point b, Point c)
        {
            double x = Math.Abs((c.Y - b.Y) * a.X - (c.X - b.X) * a.Y + c.X * b.Y - c.Y * b.X);
            double y = Math.Sqrt(Math.Pow((c.Y - b.Y), 2) + Math.Pow(c.X - b.X, 2));
            return x / y;
        }
    }
}
