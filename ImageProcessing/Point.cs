﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing
{
    public class Point
    {
        public Point()
        {

        }
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        //This method checks if the distance between two points is smaller than the Pen.Step
        public bool Equals(Point other)
        {
            if (other == null) return false;
            return (Math.Abs(other.X - X) < Pen.Step) && (Math.Abs(other.Y - Y) < Pen.Step);
        }
        public double X { get; set; }
        public double Y { get; set; }

        public override string ToString() => $"x: {X},  y: {Y}";
    }
}