﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageProcessing
{
    public class Character
    {
        public double Width { 
            get
            {
                return Points.Max(p => p.X);
            }
        }

        public List<Point> Points;
        public char Char { get; set; }

        public Character(char Char)
        {
            Points = new List<Point>();
            this.Char = Char;
        }
    }
}
