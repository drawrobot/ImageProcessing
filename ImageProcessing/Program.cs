﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ImageProcessing
{
    public class Program
    {
        //private const double WIDTH = 1160;
        private const double WIDTH = 1160;

        private static void Main(string[] args)
        {
            Pen p = new Pen();
            int X = 200;
            int Y = 200;
            string temp;
            Console.Write($"Enter X (default = {X}): ");
            temp = Console.ReadLine();
            X = temp != "" ? int.Parse(temp) : X;
            Console.Write($"Enter Y (default = {Y}): ");
            temp = Console.ReadLine();
            Y = temp != "" ? int.Parse(temp) : Y;

            p.L = Math.Sqrt(X * X + Y * Y);
            p.R = Math.Sqrt(Y * Y + (WIDTH - X) * (WIDTH - X));

            Console.WriteLine("startPoint: " + CalcPoint(p.L, p.R));
            List<short> commands = new List<short>();

            commands.AddRange(DrawImage("turm.jpg", p));
            //commands.AddRange(p.SegmentedLineTo(new Point(X, Y)));
            //commands.AddRange(p.SegmentedLineTo(new Point(X + 100, Y)));
            //commands.AddRange(p.SegmentedLineTo(new Point(X + 100, Y + 100)));
            //commands.AddRange(p.SegmentedLineTo(new Point(X, Y + 100)));
            //commands.AddRange(p.SegmentedLineTo(new Point(X, Y)));
            SaveToFile(Math.Sqrt(X * X + Y * Y), Math.Sqrt(Y * Y + (WIDTH - X) * (WIDTH - X)), commands);
            DisplayStats(commands.Count);
        }

        private static void DisplayStats(int count)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"commands: {count}, approximately {count / 84}sek");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("Finished generation...");
            Console.ReadLine();
        }

        private static List<short> DrawImage(string path, Pen p)
        {
            Image image = new Image(path, 200);
            return image.DrawImage(p);
        } 

        private static void SaveToFile(double l, double r, List<short> commands)
        {
            StreamWriter sw = new StreamWriter("tmp.txt");
            sw.WriteLine(Math.Round(WIDTH, 0));
            sw.WriteLine(Math.Round(l, 0));
            sw.WriteLine(Math.Round(r, 0));
            foreach (var x in commands)
            {
                sw.WriteLine(x);
            }
            sw.Close();
        }

        public static Point CalcPoint(double l, double r)
        {
            Point point = new Point { X = (WIDTH / 2.0) + (l * l - r * r) / (2 * WIDTH) };
            point.Y = Math.Sqrt(l * l - point.X * point.X);
            return point;
        }

        private static List<short> DrawText(string text, Pen p)
        {
            Alphabet a = new Alphabet();
            return a.DrawText(text, p);
        }
    }
}